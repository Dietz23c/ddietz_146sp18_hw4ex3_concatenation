/*
On average, an iterative algorithm takes less time compared
to the recursive algorithm. This agrees with what was discussed
in class regarding worst-case performance between each method because
the results convay a longer run-time of recursion. The run-time is longer
as the number of "items" increase.
*/
import java.util.Arrays;

/**
 * Application that takes the values of an array of ints and
 * concatenates them together, such that each int value is followed
 * by a space. 
 * @ddietz@email.sc.edu
 * @version CSCI 146 Spring 2018 Homework 4
 */


public class ConcatenateArrayTestApp {
    
    /**
     * Main method for the app
     * @param args 
     */
    public static void main( String[] args )
    {
        int[] myArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        
        // compute time for iterative version of algorithm
        long start = System.nanoTime();
        String iterativeResult = concatIterative( myArray );
        long end = System.nanoTime();
        
        long start2 = System.nanoTime();
        String recursiveResult = concatRecursive( myArray );
        long end2 = System.nanoTime();
        
                
        System.out.println("Iterative results:");
        System.out.println("------------------");
        System.out.println("Concatenated array elements: " + iterativeResult);
        System.out.println("Time, in nanoseconds: " + (end - start) );
        
        // blank line for readability
        System.out.println();
        
         // compute time for recursive version of algorithm
        
        System.out.println("Recursive results:");
        System.out.println("------------------");
        System.out.println("Concatenated array elements: " + recursiveResult);
        System.out.println("Time, in nanoseconds: " + (end2 - start2) );
        
        // compute time for recursive version of algorithm
        
    
    } // end method main
    
    /**
     * Recursive method to concatenate the elements of an array of ints
     * @param arrayOfInts
     * @return the concatenation result
     */
   
    public static String concatRecursive( int[] arrayOfInts )
    {
        if( arrayOfInts.length == 0){
            return "";
            
        } // end if 
        
        //recursion of array by creating a newly formed array each time 
        // with a new size for each.
        
        int[] newArray = Arrays.copyOfRange(arrayOfInts, 1, arrayOfInts.length);
        return arrayOfInts[0] + " " + concatRecursive(newArray) ;

        
    } // end method concatRecursive
    
    /**
     * Iterative method to concatenate the elements of an array of ints
     * @param arrayOfInts
     * @return the concatenation result
     */
    public static String concatIterative( int[] arrayOfInts )
    {
        String result = "";
        for ( int i = 0; i < arrayOfInts.length; i++ )
        {  
            result += arrayOfInts[i] + " ";
        } // end for
        return result;
        
    } // end method concatIterative
    
} // end class ConcatenateArrayTestApp
